const { option } = require("grunt");

module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'CSS',
                    src: ['*.scss'],
                    dest: 'CSS',
                    ext: '.css'
                }]
            }
        },
        watch: {
            file: ['CSS/*.scss'],
            task: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'CSS/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                }
            },


            options: {
                watchTask: true,
                server: {

                    baseDir: './'
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch')
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-sync');
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
};